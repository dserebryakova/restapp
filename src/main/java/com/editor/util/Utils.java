package com.editor.util;

import java.util.Date;

import com.editor.model.ErrorMessage;
import com.editor.dto.ErrorMessageDto;
import com.editor.dto.ProfileDto;
import com.editor.model.Profile;

public class Utils {

  public static Profile toProfile(ProfileDto profileDto) {
    return toLowerCase(new Profile(null, profileDto.getName(), profileDto.getEmail(), profileDto.getAge(), new Date()));
  }

  private static Profile toLowerCase(Profile profile) {
    profile.setEmail(profile.getEmail().toLowerCase());
    profile.setName(profile.getName().toLowerCase());
    return profile;
  }

  public static ErrorMessageDto toMessageDto(ErrorMessage errorMessage) {
    return (errorMessage == null) ? null : new ErrorMessageDto(errorMessage.getMessage(), errorMessage.getCreated());
  }

  private Utils() {
  }
}
