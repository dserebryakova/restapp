package com.editor.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.editor.model.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {

  Profile findTopByOrderByCreatedDesc();

  Optional<Profile> findByEmailIgnoreCase(String aEmail);
}
