package com.editor.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.editor.model.ErrorMessage;

@Repository
public interface ErrorRepository extends JpaRepository<ErrorMessage, Long> {

  ErrorMessage findTopByOrderByCreatedAsc();

}
