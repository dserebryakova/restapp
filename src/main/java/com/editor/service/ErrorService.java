package com.editor.service;

import com.editor.dto.ErrorMessageDto;

public interface ErrorService {

  ErrorMessageDto getMessage();

  void log(String message);
}
