package com.editor.service.impl;

import java.util.Date;

import com.editor.dto.ErrorMessageDto;
import com.editor.model.ErrorMessage;
import com.editor.repository.ErrorRepository;
import com.editor.util.Utils;
import org.springframework.stereotype.Service;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.editor.service.ErrorService;

@Service
@Slf4j
@AllArgsConstructor
public class ErrorServiceImpl implements ErrorService {

  private ErrorRepository errorRepository;

  @Override
  public ErrorMessageDto getMessage() {
    ErrorMessageDto errorMessageDto = Utils.toMessageDto(errorRepository.findTopByOrderByCreatedAsc());
    log.info("Last error is {}", errorMessageDto);
    return errorMessageDto;
  }

  @Override
  public void log(String message) {
    ErrorMessage errorMessage = new ErrorMessage(null, message, new Date());
    errorRepository.save(errorMessage);
    log.info("Created error is {}", errorMessage);
  }
}
